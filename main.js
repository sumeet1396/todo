window.onload = function() {
  var toDoContainer = document.querySelector('.todo-list'),
			inputBox = document.querySelector('.get-to-do'),
			clear = document.querySelector('.clear'),
			data = localStorage.getItem("TODO"),
			list = [],
			id = 0,
			allCount = [],
			activeCount = [],
			completedCount = [],
			controls = document.createElement('div'),
			allBtn = document.createElement('span'),
			activeBtn = document.createElement('span'),
			completedBtn = document.createElement('span'),
			validCheck = /^[A-Za-z ]{3,15}$/;

	// get item from localstorage
	var data = localStorage.getItem('TODO');

	// check if data is not empty
	if(data){
		list = JSON.parse(data);
		id = list.length; // set the id to the last one in the list
		loadList(list); // load the list to the user interface
	}
	else{
		// if data isn't empty
		list = [];
		id = 0;
	}
	
	// check if task is completed or not
	var checkPara = toDoContainer.querySelectorAll('span');
	var checkBtn = toDoContainer.querySelectorAll('input');
	for(var i = 0; i < list.length; i++){
		if(list[i].done == true){
			checkPara[i].classList.add('done-todo');
			checkBtn[i].checked = true;
		}
		else {
			checkPara[i].classList.remove('done-todo');
			checkBtn[i].checked = false;
		}
	}

	// load items to the user's interface
	function loadList(array){
		array.forEach(function(item){
				addToDo(item.name, item.id, item.done, item.trash, item.date);
		});
	}

	// creating control section
	controls.classList.add('control-block');
	allBtn.classList.add('all-btn');
	activeBtn.classList.add('active-btn');
	completedBtn.classList.add('completed-btn');

	function count(){
		allCount = [];
		completedCount = [];
		activeCount = [];
		for(var i = 0; i<list.length; i++){
			if(list[i].done == true){
				completedCount.push('1');
			}
			else{
				activeCount.push('1');
			}
			allCount.push('1');
		}

		allBtn.innerHTML = 'All: '+ list.length;
		activeBtn.innerHTML = 'Active: '+ activeCount.length;
		completedBtn.innerHTML = 'Completed: '+ completedCount.length;
	}
	count();

	controls.append(allBtn,activeBtn,completedBtn);

	// clear the local storage
	clear.addEventListener('click', function(){
		localStorage.clear();
		location.reload();
		count();
	});

	function dragStart(e) {
		this.classList.add('drag-li');
	  dragSrcEl = this;
	  e.dataTransfer.effectAllowed = 'move';
	  e.dataTransfer.setData('text/html', this.innerHTML);
	};

	function dragEnter(e) {
	  this.classList.add('over');
	}

	function dragLeave(e) {
	  e.stopPropagation();
	  this.classList.remove('over');
	}

	function dragOver(e) {
	  e.preventDefault();
	  e.dataTransfer.dropEffect = 'move';
	  return false;
	}

	function dragDrop(e) {
	  if (dragSrcEl != this) {
	    dragSrcEl.innerHTML = this.innerHTML;
	    this.innerHTML = e.dataTransfer.getData('text/html');
	  }
	  return false;
	}

	function dragEnd(e) {
	  var listItens = document.querySelectorAll('.todo-list li');
	  [].forEach.call(listItens, function(item) {
	    item.classList.remove('over');
	  });
	  this.classList.remove('drag-li');
	}
		
	// drag events
	toDoContainer.addEventListener('mouseover',function(e){
		var li = e.target;
		
		li.addEventListener('dragstart', dragStart);
	  li.addEventListener('dragenter', dragEnter);
	  li.addEventListener('dragover', dragOver);
	  li.addEventListener('dragleave', dragLeave);
	  li.addEventListener('drop', dragDrop);
	  li.addEventListener('dragend', dragEnd);
	});
	  

	// add to do function
	function addToDo(toDo, id, completed, trash, createdTime){
		if(trash){ return; }
		var item = `<li class="item cf" draggable="true" job="drag">
									<input type="radio" class="done-sign" job="complete" data-id="${id}">
									<span>${toDo}</span>
									<a href class="remove" job="delete" data-id="${id}">X</a>
									<p class="time">Created at: ${createdTime}</p>
								</li>
								`;
		
		toDoContainer.insertAdjacentHTML("beforeend", item);
		toDoContainer.appendChild(controls);
		count();
	}
	
	// create to do list
  document.addEventListener('keyup',function(e){
    e.preventDefault();
    if (e.keyCode == 13 && inputBox == document.activeElement){
      if(validCheck.test(inputBox.value)) {
				var inputText = inputBox.value,
						dateTime = new Date().toLocaleString();

				// if the input isn't empty
        if(inputText){
					
					list.push({
							name : inputText,
							id : id,
							done : false,
							trash : false,
							date: dateTime,
							completed: "none"
					});

					addToDo(inputText, id, false, false, dateTime);
					
					// add item to localstorage ( this code must be added where the LIST array is updated)
					localStorage.setItem('TODO', JSON.stringify(list));
					id++;
				}
				inputBox.value = '';
			}
			else {
				if(inputBox.value == ''){
					alert('Cant Leave The Text Box Empty Add A Value')
				}
				else {
					alert('Add A Valid Value');
				}
			}
		}
	});

	// complete to do
	function completeToDo(element){
		var nextItem = element.nextElementSibling,
				parent = element.parentNode;
				para = parent.querySelector('p');
				tempID = element.getAttribute('data-id'),
				dateTime = new Date().toLocaleString();

		if(nextItem.classList.contains('done-todo')){
			nextItem.classList.remove('done-todo');
			para.innerHTML = 'Created at:' + list[tempID].date;
			list[tempID].done = false;
			element.checked = false;
		}
		else {
			nextItem.classList.add('done-todo');
			para.innerHTML = 'Completed at:' + dateTime;
			list[tempID].completed = dateTime;
			list[tempID].done = true;
		}
		localStorage.setItem("TODO", JSON.stringify(list));
		count();
	}

	// remove to do
	function removeToDo(element){
		element.parentNode.parentNode.removeChild(element.parentNode);
		var tempID = element.getAttribute('data-id');
		list[tempID].trash = true;
		list.splice(tempID,1);

		count();
	}

	// target the items created dynamically

	toDoContainer.addEventListener("click", function(event){
		var element = event.target; // return the clicked element inside list
		var elementJob = element.attributes.job.value; // complete or delete
		
		if(elementJob == "complete") {
			completeToDo(element);
		}else if(elementJob == "delete") {
			removeToDo(element);
		}
		else if(elementJob == "drag") {
			element.addEventListener('dragstart', dragStart);
	  	element.addEventListener('dragenter', dragEnter);
	  	element.addEventListener('dragover', dragOver);
	  	element.addEventListener('dragleave', dragLeave);
	  	element.addEventListener('drop', dragDrop);
	  	element.addEventListener('dragend', dragEnd);
		}
		else {
			console.warn('nothing');
		}
		
		// add item to localstorage ( this code must be added where the LIST array is updated)
		localStorage.setItem("TODO", JSON.stringify(list));
	});
}